#! /usr/bin/env python3

import logging
logging.basicConfig(level=logging.ERROR)

import yaml
import config as c
import importlib

import os
import random
random.seed(os.urandom(10))

import sys
import traceback
from copy import deepcopy

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
updater = Updater(token=yaml.load(open('.env.yaml'))['token'])

def get_handler(module, handler_type):
    if module.__name__ not in get_handler.handlers:
        def handler(bot, update):
            answer = module.answer(update.message.text)
            if not answer:
                return
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=answer, parse_mode='markdown')
        get_handler.handlers[module.__name__] = handler_type(handler)
    return get_handler.handlers[module.__name__]
get_handler.handlers = dict()

def get_message_handler(module):
    return get_handler(module, lambda answer: MessageHandler([Filters.text], answer))

def get_command_handler(module, command):
    return get_handler(module, lambda answer: CommandHandler(command, answer))

all_modules = {
    'answer-functions': {},
    'command-functions': {},
}
all_handlers = {
    'answer-functions': {},
    'command-functions': {},
}

def reload_config():
    old_config = deepcopy(c.config)
    c.config = yaml.load(open('config.yaml'))
    def reload_handlers(htype, get_handler_func):
        modules = all_modules[htype]
        handlers = all_handlers[htype]
        if htype in old_config:
            for name, module_conf in old_config[htype].items():
                if module_conf.get('disabled', False):
                    continue
                module = modules[name]
                command = module_conf.get('command', None)
                updater.dispatcher.remove_handler(get_handler_func(module, command),
                                                  group=module_conf['group'])
                del modules[name]
                del get_handler.handlers[module.__name__]
        if htype in c.config:
            errorneous_modules = list()
            for name, module_conf in c.config[htype].items():
                if module_conf.get('disabled', False):
                    logger.info('Module {} is disabled'.format(name))
                    continue
                try:
                    modules[name] = module = importlib.import_module(name)
                    importlib.reload(module)
                    command = module_conf.get('command', None)
                    updater.dispatcher.add_handler(get_handler_func(module, command),
                                                   group=module_conf['group'])
                    logger.info('Module {} has been successfully loaded'.format(name))
                except:
                    logger.warning('Module {} has not been loaded!'.format(name))
                    traceback.print_exc()
                    errorneous_modules.append(name)
            for name in errorneous_modules:
                del c.config[htype][name]
        logger.info('{htype} succesfully reloaded'.format(**locals()))
    reload_handlers('answer-functions', lambda name, command: get_message_handler(name))
    reload_handlers('command-functions', get_command_handler)

reload_config()

def run():
    updater.start_polling()

def stop():
    updater.stop()

def restart():
    stop()
    run()

if __name__ == "__main__":
    run()
