import config as c
import random

config = c.config['answer-functions'][__name__]

def answer(text):
    i = text.rfind('?')
    if i == -1:
        return
    if not any(c.isalnum() for c in text[i + 1:]):
        return random.choice(config['answers'])
