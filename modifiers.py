import random
import logging
import config as c
import time

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def sometimes(p):
    def deco(func):
        def wrapper(*args, **kwargs):
            if (random.random() < p):
                return func(*args, **kwargs)
        return wrapper
    return deco

def throttle(dt):
    def deco(func):
        def wrapper(*args, **kwargs):
            cur_time = time.time()
            if cur_time - wrapper.last_time > dt:
                wrapper.last_time = cur_time
                return func(*args, **kwargs)
        wrapper.last_time = 0
        return wrapper
    return deco
