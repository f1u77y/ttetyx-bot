
def tokens(text):
    return [toalnum(s) for s in text.split() if toalnum(s)]

def toalnum(s):
    return ''.join(c for c in s if c.isalnum()).lower()
