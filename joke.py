import random
import config as c
config = c.config['command-functions'][__name__]

import vk
import yaml
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

vk_api = vk.API(vk.Session())
jokes = set()

def to_text(post):
    return post['text'].replace('<br>', '\n')

def jokes_with_comments(wall_id, count, offset, min_likes):
    jokes = set()
    posts = vk_api.wall.get(owner_id=source['wall-id'], count=count,
                            offset=offset)[1:]
    for post in posts:
        if len(post.get('attachments', [])) != 0:
            continue
        text = to_text(post)
        if len(text) == 0:
            continue
        jokes.add(text)
        comments = vk_api.wall.getComments(owner_id=wall_id, post_id=post['id'],
                                           need_likes=True, count=100, preview_length=0)
        for comment in comments[1:]:
            if comment['likes']['count'] >= min_likes:
                jokes.add(text)
    return jokes


for source in config['sources']:
    if source['type'] == 'vk':
        count = source['count']
        offset = 0
        while count > len(jokes):
            ccount = min(count - len(jokes), 100)
            cur_jokes = jokes_with_comments(source['wall-id'], ccount, offset,
                                            source['min-likes'])
            logger.debug('offset={}'.format(offset))
            logger.debug('len(posts)={}'.format(len(jokes)))
            offset += ccount
            jokes.update(cur_jokes)
    jokes = list(jokes)

logger.debug('len(set(jokes))) = {}'.format(len(set(jokes))))
logger.debug('len(jokes)) = {}'.format(len(jokes)))

def answer(text):
    return random.choice(jokes)
