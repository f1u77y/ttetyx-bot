from modifiers import sometimes, throttle
from utils import tokens
import config as c

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

config = c.config['answer-functions'][__name__]

def huificate_word(w):
    vmap = huificate_word.vmap
    vowels = ''.join(vmap.keys()) +''.join(vmap.values())

    for i in range(len(w)):
        if w[i] in vowels:
            return w + '-ху' + vmap.get(w[i], w[i]) + w[i+1:]
    return w
huificate_word.vmap = {
    'а': 'я',
    'э': 'е',
    'у': 'ю',
    'о': 'ё',
    'ы': 'и',
}


@throttle(config['frequency'])
def answer(s):
    hw = 0
    ws = []
    for token in tokens(s):
        w = huificate_word(token)
        if w != token:
            hw += 1
        ws.append(w)
    if hw == 0:
        return None
    return ' '.join(ws)
