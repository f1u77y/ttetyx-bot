import config as c

def code(text):
    return '```\n' + text + '```'

def dot(g):
    def deco(f):
        def wrapper(*args, **kwargs):
            return g(f(*args, **kwargs))
        return wrapper
    return deco

@dot(code)
def answer(text):
    disabled_modules = list()
    enabled_modules = list()
    status = list()
    for module_type, modules in c.config.items():
        for name, conf in modules.items():
            if conf.get('disabled', False):
                disabled_modules.append('    ' + name)
            else:
                enabled_modules.append('    ' + name)
    return '\n'.join(['Enabled modules:'] +
                     enabled_modules +
                     ['Disabled modules:'] +
                     disabled_modules)
