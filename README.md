# Steps to run
* Install requirements with `pip3 install -r requirements.txt`
* Add `token: YOUR_BOT_TOKEN_HERE` to .env.yaml

# Requirements
* python3
* python packages in requirements.txt
