from utils import tokens
import config as c

config = c.config['answer-functions'][__name__]

def answer(text):
    toks = tokens(text)
    for last, ans in config['answers'].items():
        last_tokens = last.split()
        if last_tokens == toks[-len(last_tokens):]:
            return ans
