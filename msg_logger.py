import config as c
config = c.config['answer-functions'][__name__]

def answer(text):
    with open(config['file'], 'a') as logfile:
        print(text, file=logfile)
